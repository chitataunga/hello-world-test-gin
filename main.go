package main

import (
	"encoding/xml"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)
type Person struct {
	XMLName xml.Name `xml:"person"`
	FirstName string `xml:"name>firstName"`
	LastName string `xml:"name>lastName"`
	DateServed time.Time `xml:"timeServed,attr"`
	Comment string `xml:",comment"`
}

func defaultHandler(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "hello user",
	})
}

func nameHandler(c *gin.Context) {
	name := c.Params.ByName("name")
	c.JSON(http.StatusOK,gin.H{
		"message" : "hello " + name,
	} )
}
func xmlHandler(c *gin.Context) {
	person := Person{ FirstName: "Testname", LastName: "test Last Name", DateServed: time.Now(), Comment: "This is a test xml file"  }
	c.XML(http.StatusOK, person)
}


func main() {
	router := gin.Default()
	router.GET("/", defaultHandler)
	router.GET("/:name", nameHandler)
	router.GET("/test/xml", xmlHandler)
	_ = router.Run(":10000")
}